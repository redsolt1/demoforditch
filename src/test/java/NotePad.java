import com.sun.jndi.toolkit.url.Uri;
import io.appium.java_client.windows.WindowsDriver;
import io.appium.java_client.windows.WindowsElement;
import org.junit.*;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.TimeUnit;

public class NotePad {

    @Test
    public void notepadTest() {
        try {
            Files.createDirectories(Paths.get("./src/test/resources/demoFolder/"));
            File file = new File("./src/test/resources/demoFolder/MyTestFile.txt");
            if (!file.exists()){
                Files.createFile(Paths.get("./src/test/resources/demoFolder/MyTestFile.txt"));
            }
            DesiredCapabilities appCapabilities = new DesiredCapabilities();
            appCapabilities.setCapability("app", "C:\\Windows\\System32\\notepad.exe");
            appCapabilities.setCapability("appArguments", "MyTestFile.txt");

            appCapabilities.setCapability("appWorkingDir", "C:\\Winium\\1");
            WindowsDriver NotepadSession = new WindowsDriver<WindowsElement>(new URL("http://127.0.0.1:4723"), appCapabilities);

            NotepadSession.findElementByClassName("Edit").sendKeys("мой первый тест");

            NotepadSession.closeApp();

            NotepadSession.findElementByName("Не сохранять").click();

        }catch(Exception e){
            e.printStackTrace();
        } finally {
        }
    }




}
