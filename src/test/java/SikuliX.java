import org.junit.Test;
import org.sikuli.script.*;

import java.awt.*;
import java.awt.event.KeyEvent;

public class SikuliX {

    private static final Pattern Calculator = new Pattern("./src/test/resources/calc/Calculator.jpg");

    private static final Pattern One = new Pattern("./src/test/resources/calc/1.jpg");
    private static final Pattern Two = new Pattern("./src/test/resources/calc/2.jpg");
    private static final Pattern Three = new Pattern("./src/test/resources/calc/3.jpg");
    private static final Pattern Plus = new Pattern("./src/test/resources/calc/+.jpg");
    private static final Pattern Equal = new Pattern("./src/test/resources/calc/=.jpg");

    @Test
    public void sikuliTest (){
        RobotDesktop robotDesktop = null;
        try {
            robotDesktop = new RobotDesktop();

        robotDesktop.keyDown(KeyEvent.VK_WINDOWS);
        robotDesktop.keyUp(KeyEvent.VK_WINDOWS);

        Screen screen = new Screen();
        screen.type("calc");
        screen.keyDown(KeyEvent.VK_ENTER);
        screen.keyUp(KeyEvent.VK_ENTER);

        screen.wait(Calculator, 5d);

        screen.wait(One.similar((float) 0.8), 5).click();
        screen.wait(Plus.similar((float) 0.8), 5).click();
        screen.wait(Two.similar((float) 0.8), 5).click();
        screen.wait(Equal.similar((float) 0.8), 5).click();

        screen.wait(Two.similar((float) 0.8), 5).click();
        screen.wait(Plus.similar((float) 0.8), 5).click();
        screen.wait(Three.similar((float) 0.8), 5).click();
        screen.wait(Equal.similar((float) 0.8), 5).click();

        } catch (AWTException e) {
            e.printStackTrace();
        } catch (FindFailed findFailed) {
            findFailed.printStackTrace();
        }

    }


}
